# Creating a repository on GitLab for another team

- Create the repo on GitLab:
    * Use `New Project`
    * Select `Create Blank Project`
    * Under project URL select `ska-telescope`
    * Set the visibility level to `Public`
    * Select `Initialize repository with a README`
- Add LICENCE to the repo:
    * Choose template `default licence`
- Under the `Members` sidetab on GitLab:
    - Add 2 members from the team with permission `Mantainer` (it is up to those members to then later add any other maintainers)
- Setup the repo on GitHub:
    - On GitHub go to `New` Repository
    - Select `Import a repository`
    - Enter the GitLab URL of the newly created repo
    - Be sure to set the owner as `ska-telescope`
    - Set the Privacy as `Public`
    - Import the repository and take note of the URL
- Under the `Settings/Repository` sidetab on GitLab:
    - Under `Mirroring Repositories` set the URL of the GitHub repo
    - Mirror direction `Push`
    - `Authentication method` password or SHA (if you have that setup on GitHub)
    - Fill in the appropriate value for your personal password or SHA pubkey.
    - Press `Mirror repository`