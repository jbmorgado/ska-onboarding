# Ansible Workshop

## Prerequisites
- EngageSKA VPN access.
- EngageSKA OpenStack credentials.
- Basic knowledge of OpenStack Dashboard usage.
- Basic knowledge of ssh.
- Basic knowledge of Ubuntu/Debian package manager.

## Getting Started

- Create an Ubuntu LTS VM on the EngageSKA [OpenStack Cluster Dashboard](http://192.168.93.215/) (*you will need to be using the IT Aveiro VPN to access the cluster*)
  * Flavour: **m2.xlarge**.
  * On *Source* tab, select a volume size of 10 GB and set *Delete Volume on Instance Delete* to *Yes*.
  * Add your personal ssh key to the OpenStack dashboard if you haven't done so already.
- Assign a Floating IP to the VM.
- Login into the newly created VM (user *ubuntu*) through VSCode remote ssh using the floating IP address.

## Install Ansible on Ubuntu 20.04 LTS
On Ubuntu 20.04 LTS and higher, updated ansible packages are readily available as part of the standard Ubuntu repositories.

```bash
# Install Ansible
sudo apt update
sudo apt install ansible
```

## Initialize a Git repo
Create a directory named `ansible-demo` and initialize it as an empty git repository:

```bash
mkdir ansible-demo
cd ansible-demo
git init
```

## Ansible basic configuration
Start by creating an `ansible.cfg` file with the contents:

```ansible
[defaults]
inventory = hosts.yml
```

## Inventory
*"Ansible works against multiple managed nodes or “hosts” in your infrastructure at the same time, using a list or group of lists know as inventory. Once your inventory is defined, you use patterns to select the hosts or groups you want Ansible to run against."*

Create the `hosts.yml` file with the contents:

```yml
all:
  children:
    vms:
      hosts:
        development:
          ansible_user: $USER
          ansible_connection: local
        prometheus:
          ansible_host: 192.168.93.26
  vars:
    ansible_python_interpreter: /usr/bin/python3
```

- Where the `ansible_user` is the user that will run the Ansible commands (i.e. probably `ubuntu` in this example).
- Where the `ansible_host` is the IP address of the VM.
- `ansible_connection` is the connection type to use (i.e. `local` in this example since we will be having ansible managing the local machine where it is running from).

Test the ansible connection is working correctly with:

```bash
ansible all -m ping
```

## Playbook
Add the playbook named `deploy_helloworld.yml` in the root folder for the repo:

```yml
---

- name: Deploy helloworld
  hosts:
    - development
  roles:
    - helloworld
```

## Roles
*"Roles are ways of automatically loading certain vars_files, tasks, and handlers based on a known file structure. Grouping content by roles also allows easy sharing of roles with other users."*

Create the `roles/helloworld/tasks` directory and inside it create the file `main.yml` with the contents:

```yml
---

- name: clone helloworld repo
  git:
    repo: https://github.com/mrsarm/helloworld-c
    dest: /tmp/helloworld
    recursive: yes
  register: helloworld_source

- name: compile and "install" helloworld
  shell: cmake . && make
  args:
    chdir: "/tmp/helloworld"
  when: helloworld_source.after != helloworld_source.before
```

Now let's run the playbook with the command:

```bash
ansible-playbook -l development deploy_helloworld.yml
```

## Variables
Create the directory `roles/helloworld/vars` and inside that directory create the file `main.yml` with the contents:

```yml
---

helloworld_compile_dir: /tmp/helloworld
```

Now alter the file `roles/helloworld/tasks/main.yml` so that we use the variable `"{{ helloworld_compile_dir }}"` instead of `/tmp/helloworld`. 

## Ansible modules
Ansible offers an array of functionality in the form of modules. We already used the **git** and **shell** module above. Let's try a few others.

### apt module
Allows to install packages using Ubuntu/Debian *apt* package manager.

Create the directory `/roles/common/tasks/` inside it, create the file `main.yml` with the contents:

```yml
---

- name: gather package facts
  package_facts:
    manager: auto

- name: install packages
  apt:
    pkg:
      - cmake
      - htop
      - inxi
      - tldr
      - vim
      - wget
    state: latest
    update_cache: yes

- name: remove dependencies that are no longer required
  apt:
    autoremove: yes

- name: upgrade installed packages
  apt:
    upgrade: 'yes'
    force_apt_get: yes
```

#### Exercise
Now, try to add a `deploy_common.yml` playbook to run the common role by yourself.

### apt_key and apt_repository modules
Adds new pubkeys and respective repositories to Ubuntu package manager.

Add a new *openscad* role with the task:

```yml
---

- name: configure OpenSCAD APT key
  apt_key:
    url: https://files.openscad.org/OBS-Repository-Key.pub
    state: present

- name: configure OpenSCAD APT repositories
  apt_repository:
    repo: deb https://download.opensuse.org/repositories/home:/t-paul/xUbuntu_20.04/ ./
    filename: openscad
    state: present

- name: install OpenSCAD
  apt:
    pkg:
      - openscad-nightly 
    state: present
    update_cache: yes
```

## Running groups of roles depending on the host
Ideally, we want to run an ansible playbook that brings several hosts to a given state without having to manually run each playbook. In order to do so, we can create a group of roles that will be run for each machine.

Create a `deploy_all.yml` file on the base project directory with the contents:

```yml
---

- name: Development playbook
  hosts: development
  remote_user: ubuntu
  roles:
    - { role: common, become: true }
    - helloworld
    # - openscad

- name: Prometheus playbook
  hosts: prometheus
  remote_user: ubuntu
  roles:
    - common
```

Run the deployment with:

```bash
ansible-playbook -l development deploy_all.yml
```
