# Gitlab CI/CD Workshop

## Adding SSH keys to GitLab

Follow the guide at: https://docs.gitlab.com/ee/ssh/ Do it from your virtual machine.

## Getting Started

- Create a new project in your personal account on GitLab (careful, don't create it in the SKA Project)
  * Project Name: cicd-test
  * Visibility: Public
  * Initialize repository with a README
- Clone it to your virtual machine

# Quick Overview of the Project

- Settings
  * Badges
  * Repository
  * CI/CD

## Get the CI pipeline started

On the root folder for the project create the file `.gitlab-ci.yml`

```yml
default:
    image: python:alpine

stages:
    - hello

hello:
    stage: hello
    script:
        - mkdir build
        - touch build/hello-world.txt
```

Push the changes to your repo on GitLab

## Artifacts

If we want to have access to files created as part of our pipeline we need to define artifacts, add the following to the `hello` stage of our pipeline:

```yml
    artifacts:
        paths:
            - build
```

## GitLab CI/CD Variables

The Gitlab API sets a number of variables accessible through its CI pipeline: https://docs.gitlab.com/ee/ci/variables/predefined_variables.html

This has a number of handy uses (for instance for our Badge Creation functionality on SKA).

```yml
default:
    image: python:alpine

stages:
    - hello
    - checkvars

hello:
    stage: hello
    script:
        - mkdir build
        - touch build/hello-world.txt

checkvars:
    stage: checkvars
    script:
        - mkdir build
        - touch build/check-vars.txt
        - echo "Commit SHA:" $CI_COMMIT_SHA >> build/check-vars.txt
        - echo "Commit Branch:" $CI_COMMIT_BRANCH >> build/check-vars.txt

    artifacts:
        paths:
            - build
```
